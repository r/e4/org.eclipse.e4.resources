/*******************************************************************************
 * Copyright (c) 2013 SAP AG.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Eduard Bartsch (SAP AG) - initial API and implementation
 *******************************************************************************/
package org.eclipse.core.resources.semantic.test;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.UUID;

import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.core.internal.resources.semantic.SemanticFileSystem;
import org.eclipse.core.resources.semantic.ISemanticFileSystem;
import org.eclipse.core.resources.semantic.spi.ISemanticFileStore;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * 
 */
public class TestSemanticFileSystem {
	private static final String TEST_FOLDER2 = "testFolder2";
	private static final String TEST2_PROJECT_XMI_FILENAME = "$.test2.xmi";
	private static final String TEST_PROJECT_XMI_FILENAME = "$.test.xmi";
	private static final String TEST_FOLDER = "testFolder";
	private static final String TEST_ROOT2 = "test2";
	private static final String TEST_ROOT = "test";
	// private static final String TEST_ROOT = "testRoot";
	// private static final String TEST_ROOT2 = "testRoot2";
	private File rootFolder;
	private SemanticFileSystem fs;

	@Before
	public void beforeMethod() throws Exception {
		this.rootFolder = createTestRoot();
	}

	@After
	public void afterMethod() throws Exception {
		deleteRecursively(this.rootFolder);
	}

	@Test
	public void test_GivenNoMetadataExists_WhenCreateSFSaddFolderAndSave_ThenMetadataFileIsCreated() throws Exception {

		// when
		fs = new SemanticFileSystem(this.rootFolder);

		IFileStore store = fs.getStore(new URI(ISemanticFileSystem.SCHEME + ":/" + TEST_ROOT));

		store.mkdir(0, null);

		((ISemanticFileStore) store).addChildFolder(TEST_FOLDER);

		// then
		File metadataLocation = new File(fs.getPathToDb());
		File testMetadataFile = new File(metadataLocation.getParentFile(), TEST_PROJECT_XMI_FILENAME);

		assertTrue(rootFolder.exists());
		assertTrue("metadata file must be created", metadataLocation.exists());
		assertTrue("project-specific metadata file must be created", testMetadataFile.exists());
	}

	@Test
	public void test_GivenNoMetadataExists_WhenCreateSFSaddFileAndSave_ThenMetadataFileIsCreated() throws Exception {

		// when
		fs = new SemanticFileSystem(this.rootFolder);

		IFileStore store = fs.getStore(new URI(ISemanticFileSystem.SCHEME + ":/" + TEST_ROOT));

		store.mkdir(0, null);

		((ISemanticFileStore) store).addChildFile("testFile");

		// then
		File metadataLocation = new File(fs.getPathToDb());
		File testMetadataFile = new File(metadataLocation.getParentFile(), TEST_PROJECT_XMI_FILENAME);

		assertTrue(rootFolder.exists());
		assertTrue("metadata file must be created", metadataLocation.exists());
		assertTrue("project-specific metadata file must be created", testMetadataFile.exists());
	}

	@Test
	public void test_GivenNoMetadataExists_WhenCreateSFSaddTwoNodesAndSave_ThenTwoMetadataFileAreCreated() throws Exception {

		// when
		fs = new SemanticFileSystem(this.rootFolder);

		IFileStore store = fs.getStore(new URI(ISemanticFileSystem.SCHEME + ":/" + TEST_ROOT));

		store.mkdir(0, null);

		((ISemanticFileStore) store).addChildFolder(TEST_FOLDER);

		store = fs.getStore(new URI(ISemanticFileSystem.SCHEME + ":/" + TEST_ROOT2));

		store.mkdir(0, null);

		((ISemanticFileStore) store).addChildFolder(TEST_FOLDER);

		// then
		File metadataLocation = new File(fs.getPathToDb());
		File testMetadataFile = new File(metadataLocation.getParentFile(), TEST_PROJECT_XMI_FILENAME);
		File test2MetadataFile = new File(metadataLocation.getParentFile(), TEST2_PROJECT_XMI_FILENAME);

		assertTrue(rootFolder.exists());
		assertTrue("metadata file must be created", metadataLocation.exists());
		assertTrue("project-specific metadata file must be created", testMetadataFile.exists());
		assertTrue("project-specific metadata file must be created", test2MetadataFile.exists());
	}

	@Test
	public void test_GivenTwoProjectsExists_WhenLoadAndChangeOnlyOneProject_ThenOnlyTwoMetadataFilesAreChanged() throws Exception {
		// given
		fs = new SemanticFileSystem(this.rootFolder);

		IFileStore store = fs.getStore(new URI(ISemanticFileSystem.SCHEME + ":/" + TEST_ROOT));

		store.mkdir(0, null);

		((ISemanticFileStore) store).addChildFolder(TEST_FOLDER);

		store = fs.getStore(new URI(ISemanticFileSystem.SCHEME + ":/" + TEST_ROOT2));

		store.mkdir(0, null);

		((ISemanticFileStore) store).addChildFolder(TEST_FOLDER);

		File metadataLocation = new File(fs.getPathToDb());
		File testMetadataFile = new File(metadataLocation.getParentFile(), TEST_PROJECT_XMI_FILENAME);
		File test2MetadataFile = new File(metadataLocation.getParentFile(), TEST2_PROJECT_XMI_FILENAME);

		long lastModification = metadataLocation.lastModified();
		long lastModification1 = testMetadataFile.lastModified();
		long lastModification2 = test2MetadataFile.lastModified();

		Thread.sleep(1011);

		// when
		fs = new SemanticFileSystem(this.rootFolder);

		store = fs.getStore(new URI(ISemanticFileSystem.SCHEME + ":/" + TEST_ROOT));

		((ISemanticFileStore) store).addChildFolder(TEST_FOLDER2);

		// then
		assertTrue("folder must exist", store.getChild(TEST_FOLDER).fetchInfo().exists());

		long newModification = metadataLocation.lastModified();
		long newModification1 = testMetadataFile.lastModified();
		long newModification2 = test2MetadataFile.lastModified();

		assertTrue("metadata file must be modified", lastModification != newModification);
		assertTrue("metadata file must be modified", lastModification1 != newModification1);
		assertTrue("metadata file must not be modified", lastModification2 == newModification2);

		fs = new SemanticFileSystem(this.rootFolder);

		store = fs.getStore(new URI(ISemanticFileSystem.SCHEME + ":/" + TEST_ROOT));

		assertTrue("folder must exist", store.getChild(TEST_FOLDER2).fetchInfo().exists());
	}

	// Helper methods

	private File createTestRoot() {
		String tmpdir = System.getProperty("java.io.tmpdir");
		String uuid = UUID.randomUUID().toString();
		File uniqueRoot = new File(tmpdir, uuid);
		uniqueRoot.deleteOnExit();
		return uniqueRoot;
	}

	private void deleteRecursively(File root) throws IOException {
		if (root.exists()) {
			if (root.isFile()) {
				root.delete();
			} else {
				File[] files = root.listFiles();
				if (files != null) {
					for (File file : files) {
						deleteRecursively(file);
					}
				}
				if (!root.delete()) {
					throw new IOException("Unable to delete " + root.getAbsolutePath());
				}
			}
		}
	}

}
